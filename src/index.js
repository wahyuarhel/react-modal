import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import "./Style/Main.scss";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
