import { useState } from "react";
import Modal from "react-modal";

const MODAL_LOGIN = 1;
const MODAL_SIGNUP = 2;

function App() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [whichModal, setWhichModal] = useState(null);

  const handleLogin = (event) => {
    event.preventDefault();
    setIsModalOpen(false);
  };

  return (
    <div>
      <h1>Learn React Modal</h1>
      <hr />
      <button
        onClick={() => {
          setIsModalOpen(true);
          setWhichModal(MODAL_LOGIN);
        }}
      >
        Login
      </button>
      <Modal
        isOpen={isModalOpen}
        onRequestClose={() => setIsModalOpen(false)}
        className="modal__container"
        overlayClassName="modal__overlay--center"
        contentLabel="Learn Modal"
      >
        {renderWhichModal()}
      </Modal>
    </div>
  );
  function renderWhichModal() {
    switch (whichModal) {
      case MODAL_SIGNUP:
        return (
          <div className="home__login">
            <h1> - MilanTV - </h1>
            <form className="" onSubmit={handleLogin}>
              <div>Full Name</div>
              <input type="text" placeholder="username or email" />
              <div>Email</div>
              <input type="email" placeholder="password" />

              <div>Password</div>
              <input type="password" placeholder="password" />

              <button className="home__login__button" type="submit">
                Sign Up
              </button>
            </form>
          </div>
        );
      case MODAL_LOGIN:
        return (
          <div className="home__login">
            <h1> - MilanTV - </h1>
            <form className="home__signup__form" onSubmit={handleLogin}>
              <div>Email</div>
              <input type="email" placeholder="password" />

              <div>Password</div>
              <input type="password" placeholder="password" />

              <button className="home__login__button" type="submit">
                Login
              </button>
            </form>
            <h4>
              don't have account,{" "}
              <span
                onClick={() => {
                  setWhichModal(MODAL_SIGNUP);
                }}
              >
                Sign Up Here
              </span>
            </h4>
          </div>
        );
      default:
        break;
    }
  }
}

export default App;
